import { AntagonClientPage } from './app.po';

describe('antagon-client App', () => {
  let page: AntagonClientPage;

  beforeEach(() => {
    page = new AntagonClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
