import { Component, OnInit } from '@angular/core';
import { Post } from '../post.model';
import { PostService } from '../post.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss'],
  providers: []
})
export class NewPostComponent implements OnInit {

  body: string;
  constructor(private postService: PostService, private authService: AuthService) { }

  ngOnInit() {}

  submit(): void {
    this.authService.getUserId().first().subscribe((id) => {
      const post: Post = new Post({
        body: this.body,
        timeCreated: new Date().toISOString(),
        author: id
      });
      this.postService.createNew(post).subscribe(() => {
        this.body = '';
      });
    })
  }

}
