// Simple User Class
export type UserId = string;

export class User {
  id?: UserId;
  name?: string;
  alias?: string;
  email?: string;

  /* tslint:disable-next-line: no-any */
  constructor(params: any) {
    Object.assign(this, params);
  }

  getPublic(): User {
    return new User({
      id: this.id,
      alias: this.alias
    });
  }
}
