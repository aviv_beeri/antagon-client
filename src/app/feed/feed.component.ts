import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../post.model';
import { PostService } from '../post.service';
import { AuthService } from '../auth.service';
import { UserService } from '../user.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css'],
  providers: []
})
export class FeedComponent implements OnInit {

  public posts: Observable<Post[]>;
  private userId: Observable<string>;

  constructor(private postService: PostService, private authService: AuthService, private userService: UserService) { }

  ngOnInit() {
    this.posts = this.postService.getPosts().map((posts) => {
      posts.forEach((post) => {
        post.authorPromise = this.userService.getUser(<string>post.author);
      });
      return posts;
    });
    this.postService.getAll();
    this.userId = this.authService.getUserId();
  }

  rate(mood: string, post: Post): void {
    this.userId.first().subscribe((userId) => {
      if (!userId) {
        return;
      }
      const oldMood: string = post.currentUserMood(userId);
      post.rate(mood, userId);
      this.postService.update(post).catch(() => {
        post.rate(oldMood, userId);
      });
    });
  }

  opinion(post: Post): string {
    let total: number = 0;
    if ((post.positiveRatings.size + post.negativeRatings.size + post.mediumRatings.size) === 0) {
      return '';
    }
    total = (post.positiveRatings.size - post.negativeRatings.size)/(post.positiveRatings.size + post.negativeRatings.size + post.mediumRatings.size);
    if (total < -0.75) {
      return 'Complete flop!';
    } else if (total >= -0.75 && total < -0.25) {
      return 'Needs work'
    } else if (total >= -0.25 && total < 0.25) {
      return 'Mediocre'
    } else if (total >= 0.25 && total < 0.75) {
      return 'Devious'
    } else {
      return 'Master plan!'
    }
  }
}
