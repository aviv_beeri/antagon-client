import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { User } from './user.model';
import { AuthService } from './auth.service';
import { environment } from '../environments/environment';

const endpoint = environment.host;
const protocol = environment.protocol;

interface UserStore {
  [id: string]: Promise<User>;
}

@Injectable()
export class UserService {

  private users: UserStore = {};
  constructor(private authService: AuthService, private http: Http) { }

  getUser(id: string): Promise<User> {
    if (!this.users[id]) {

      this.users[id] = this.http.get(`${protocol}://${endpoint}/users/${id}`, { headers: this.authService.getHeaders() })
        .toPromise()
        .then((response) => response.json());
    }

    return this.users[id];

  }
}
