// Simple Post Class
import { User } from './user.model';
export type UserId = string;

interface Ratings {
  positive?: UserId[];
  negative?: UserId[];
  medium?: UserId[];
}

export class Post {
  id?: string;
  body: string;
  timePosted: string;
  author: UserId;
  authorPromise?: Promise<User>;
  positiveRatings?: Set<UserId>;
  negativeRatings?: Set<UserId>;
  mediumRatings?: Set<UserId>;
  // comments: Comments
 /* tslint:disable-next-line: no-any */
  constructor(params: any) {
    Object.assign(this, params);
    const ratings: Ratings = params.ratings || {};
    this.positiveRatings = new Set(ratings.positive || []);
    this.negativeRatings = new Set(ratings.negative || []);
    this.mediumRatings = new Set(ratings.medium || []);
  }

  unrate(userId: string): void {
    this.positiveRatings.delete(userId);
    this.negativeRatings.delete(userId);
    this.mediumRatings.delete(userId);
  }

  rate(mood: string, userId: string): void {
    if (mood === this.currentUserMood(userId)) {
      this.unrate(userId);
      return;
    }
    switch (mood) {
      case 'positive':
        this.unrate(userId);
        this.positiveRatings.add(userId);
        break;
      case 'negative':
        this.unrate(userId);
        this.negativeRatings.add(userId);
        break;
      case 'medium':
        this.unrate(userId);
        this.mediumRatings.add(userId);
        break;
      default: throw new Error(`Invalid mood used for post with id ${this.id}`);
    }
  }

  currentUserMood(userId: string): string {
    if (this.positiveRatings.has(userId)) {
      return 'positive';
    } else
    if (this.mediumRatings.has(userId)) {
      return 'medium';
    } else
    if (this.negativeRatings.has(userId)) {
      return 'negative';
    } else {
      return'none';
    }
  }

  serialise(): any {
    const output: any = JSON.parse(JSON.stringify(this));
    output.ratings = {};

    output.ratings.positive = Array.from(this.positiveRatings)
    output.ratings.medium = Array.from(this.mediumRatings)
    output.ratings.negative = Array.from(this.negativeRatings)
    delete output.authorPromise;
    return JSON.stringify(output);
  }
}
