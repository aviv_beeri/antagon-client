import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { Post } from './post.model';
import { AuthService } from './auth.service';
import { environment } from '../environments/environment';

const endpoint = environment.host;
const protocol = environment.protocol;
@Injectable()
export class PostService {

  private posts: BehaviorSubject<Array<Post>> = new BehaviorSubject<Array<Post>>([]);

  constructor(private http: Http, private authService: AuthService) {
    this.getAll();
  }

  getAll(): Promise<Post[]> {
    return this.http.get(`${protocol}://${endpoint}/posts`)
      .toPromise()
    .then((response) => response.json())
    .then((data) => data.map((item) => new Post(item)))
    .then((posts) => {
      this.posts.next(posts);
      return posts;
    });
  }

  getPosts(): Observable<Array<Post>> {
    return this.posts.asObservable();
  }

  createNew(post: Post): Observable<Post> {
    const obs: Observable<Post> = this.http.post(`${protocol}://${endpoint}/posts`, JSON.stringify(post), { headers: this.authService.getHeaders()}).share().map((response) => response.json() as Post);

    obs.subscribe((res) => {
      this.posts.value.unshift(new Post(res));
      this.posts.next(this.posts.value);
    });
    return obs;
  }

  update(post: Post): Promise<Post> {
    return this.http.put(`${protocol}://${endpoint}/posts/${post.id}`, post.serialise(), { headers: this.authService.getHeaders() })
    .toPromise().then((response) => {
      return response.json();
    }).then((body) => {
      return new Post(body);
    });
  }
}
