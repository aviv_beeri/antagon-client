import { NgModule, Type } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MdButtonModule,
  MdCardModule,
  MdIconModule,
  MdToolbarModule,
  MdInputModule
} from '@angular/material';

const modules: Type<any>[] = [
  MdButtonModule,
  MdCardModule,
  MdIconModule,
  MdToolbarModule,
  MdInputModule,
  NoopAnimationsModule
];

@NgModule({
  imports: modules,
  exports: modules,
  declarations: []
})
export class UIModule { }
