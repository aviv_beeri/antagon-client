import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Headers } from '@angular/http';
import { User } from './user.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { environment } from '../environments/environment';

const endpoint = environment.host;
const protocol = environment.protocol;

@Injectable()
export class AuthService {

  user: BehaviorSubject<firebase.User>;
  token: string;

  constructor(private afAuth: AngularFireAuth, private http: Http) {
    this.user = new BehaviorSubject<firebase.User>(afAuth.auth.currentUser);
    afAuth.authState.subscribe((newUser) => {
      this.user.next(newUser);
      this.token = null;
      if (newUser) {
        newUser.getIdToken().then((token) => {
          this.token = token;
        });
      }
    });
  }

  login(email: string, password: string): Promise<firebase.User> {
    return Promise.resolve(this.afAuth.auth.signInWithEmailAndPassword(email, password));
  }

  register(email: string, password: string, user: User): Promise<firebase.User> {
    return Promise.resolve(this.afAuth.auth.createUserWithEmailAndPassword(email, password))
      .then((firebaseUser) => {
        const newUser: User = new User(user);
        newUser.id = firebaseUser.uid;
        let tokenPromise = Promise.resolve(this.token);
        if (!this.token) {
          tokenPromise = firebaseUser.getIdToken();
        }
        return tokenPromise.then((token) => {
          this.token = token;
        }).then(() => this.http.put(`${protocol}://${endpoint}/users/${firebaseUser.uid}`, newUser, { headers: this.getHeaders() })
          .toPromise())
          .then(() => firebaseUser)
          .catch(() => console.error('USER MUST BE DELETED. ORPHAN!'));
      });
  }

  isAuthenticated(): Observable<any> {
    return this.afAuth.authState;
  }

  getUserId(): Observable<string> {
    return this.user.asObservable().map((user) => user ? user.uid : null);
  }

  signOut() {
    return this.afAuth.auth.signOut();
  }

  getHeaders(): Headers {
    return new Headers({
      'Authorization': `Bearer ${this.token}`,
      'Content-Type': 'application/json'
    });
  }
}
