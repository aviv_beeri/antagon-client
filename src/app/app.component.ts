import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Antagon';
  userId: Observable<string>;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.userId = this.authService.getUserId();
  }

  logout() {
    this.authService.signOut();
  }
}
