import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  message: string;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.signOut();
  }

  login(values: any): void {
    this.authService.login(values.email, values.password)
      .then(() => {
        this.router.navigate(['']);
      }).then(() => {
        this.router.navigate(['']);
      }).catch((reason) => {
        if (reason.code === '') {
          // Do some custom handling if we want to rename certain messages.
        } else if (reason.message) {
          this.message = reason.message;
        } else {
          this.message = 'Something went wrong. Please try again, or contact support';
        }
      });
  }

}
