import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { User } from '../user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  message: string;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {}

  register(values: any): void {
    this.authService.register(values.email, values.password, new User( {
      name: values.name,
      alias: values.alias,
      email: values.email
    }))
    .then(() => {
      this.router.navigate(['']);
    }).catch((reason) => {
      if (reason.code === '') {
        // Do some custom handling if we want to rename certain messages.
      } else if (reason.message) {
        this.message = reason.message;
      } else {
        this.message = 'Something went wrong. Please try again, or contact support';
      }
    });
  }

}
