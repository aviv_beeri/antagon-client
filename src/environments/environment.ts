// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  host: 'localhost:8080',
  protocol: 'http',
  firebase: {
    apiKey: 'AIzaSyCrvIk_Hw4zPXliX7E1t_zWQ3T-3i3qZak',
    authDomain: 'antagon-12b6b.firebaseapp.com',
  }
};
